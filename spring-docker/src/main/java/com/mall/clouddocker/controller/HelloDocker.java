package com.mall.clouddocker.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cheng
 */
@RestController
@RequestMapping("/index")
public class HelloDocker {

    @RequestMapping("/getUserName")
    public String getUserName(){
        return "访问成功，您好！";
    }
}
