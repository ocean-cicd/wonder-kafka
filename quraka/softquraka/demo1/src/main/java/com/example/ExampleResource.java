package com.example;

import com.example.dto.UserDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class ExampleResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello from RESTEasy Reactive";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getUser")
    public UserDto getUserInfo(){
        UserDto userDto = new UserDto();
        userDto.setId(120123L);
        userDto.setAddress("山东济南市历下区");
        userDto.setUsername("张明");
        userDto.setPhone("15690812981");
        return userDto;
    }
}