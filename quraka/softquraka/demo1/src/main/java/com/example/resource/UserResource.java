package com.example.resource;

import com.example.dto.UserDto;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@ApplicationScoped
@Path("/user")
public class UserResource {


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public UserDto getUserByName(){

        UserDto userDto = new UserDto();
        userDto.setId(120123L);
        userDto.setAddress("山东济南市历下区");
        userDto.setUsername("张明");
        userDto.setPhone("15690812981");
        return userDto;
    }
}
