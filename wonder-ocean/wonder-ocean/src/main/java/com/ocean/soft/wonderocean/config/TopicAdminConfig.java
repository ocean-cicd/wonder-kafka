package com.ocean.soft.wonderocean.config;

import jakarta.annotation.PostConstruct;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.support.GenericWebApplicationContext;

import java.util.List;

/**
 * 根据yaml配置信息自动创建topic
 */
@Configuration
@SuppressWarnings("all")
public class TopicAdminConfig {

    private final TopicConfigurations configurations;

    private final GenericWebApplicationContext context;

    public TopicAdminConfig(TopicConfigurations configurations, GenericWebApplicationContext context) {
        this.configurations = configurations;
        this.context = context;
    }

    @PostConstruct
    public void init() {
        initializeBeans(configurations.getTopics());
    }

    public void initializeBeans(List<TopicConfigurations.Topic> topics) {
        topics.forEach(t -> context.registerBean(t.name, NewTopic.class, t::toNewTopic));
    }


}
