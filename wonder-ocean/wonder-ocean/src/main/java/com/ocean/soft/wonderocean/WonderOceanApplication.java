package com.ocean.soft.wonderocean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cheng
 */
@SpringBootApplication
public class WonderOceanApplication {

    public static void main(String[] args) {
        SpringApplication.run(WonderOceanApplication.class, args);
    }

}
