package com.ocean.soft.wonderocean.service;

public class MallObjectLock implements Runnable{

    static MallObjectLock objectLock = new MallObjectLock();

    final Object block1 = new Object();
    final Object block2 = new Object();


    @Override
    public void run() {
        synchronized (block1) {
            System.out.println("第一把锁，block1，我是线程: " + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("block1锁，" + Thread.currentThread().getName() + "结束");
        }
        synchronized (block2) {
            System.out.println("block2,线程名称: " + Thread.currentThread().getName());
            try {
                block2.wait(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("block2锁，" + Thread.currentThread().getName() + "结束");
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(objectLock);
        Thread t2 = new Thread(objectLock);

        t1.start();
        t2.start();
    }
}
