package com.ocean.soft.wonderocean.config;

import lombok.Data;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "kafka")
@Data
public class TopicConfigurations {

    private List<Topic> topics;

    @Data
    static class Topic{
        String name;
        Integer numPartitions = 3;
        Short replicationFactor = 1;

        NewTopic toNewTopic(){
            return new NewTopic(this.name,this.numPartitions,this.replicationFactor);
        }
    }
}
