package com.ocean.soft.wonderocean.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Value("${jmx.username}")
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @GetMapping(value = "/getUser")
    public String getUserInfo(){
        return """
                2022年中国新冠疫情经历了多次反复
                最终在年底放开了封锁
                """;

    }
    @GetMapping("/getRiver")
    public String getRiverName(){
        return "永定河" + username;
    }
}
